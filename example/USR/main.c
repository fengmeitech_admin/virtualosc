/******************************************************************
 * 文件：main.c
 * 功能：主函数入口
 * 日期：2018-02-16
 * 作者：zx
 * 版本：Ver.1.0 | 最初版本
 * 官博：http://fengmeitech.club
 * Copyright (C) 2017 zx. All rights reserved.
*******************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "DELAY/Delay.h"
#include "DHT11/DHT11.h"
#include "LED/LED.h"
#include "UART/uart.h"
#include <string.h>
#include "virtualosc.h"
#include "math.h"

int main(void)
{
	static unsigned int i = 0;
	float point;
	
	/*初始化各外设*/ 
	initSysTick(); 
	initUART();
	initLED();
	
	while (1)
	{
		point = 125*sin(i++*3.1415926/180)+125;   			//正弦波
		updateVirtualOSC((char *)&point, sizeof(point));	//此时上传的单点 也可以上传float数组
		Delay_ms(20);  										//50Hz你
		toggleLED();
	}
}

